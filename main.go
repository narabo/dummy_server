package main

import (
	"encoding/json"
	"net/http"
	"os/exec"

	"github.com/gorilla/mux"
)

type User struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/api/signup", signupHandler)
	http.Handle("/", r)
	http.ListenAndServe(":4300", nil)
}

func signupHandler(w http.ResponseWriter, req *http.Request) {
	var user User

	decoder := json.NewDecoder(req.Body)
	if err := decoder.Decode(&user); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if result := initDir(user.Name); result != true {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

}

func initDir(name string) bool {
	var cmd = "cp -r initDir " + name

	if err := exec.Command("sh", "-c", cmd).Run(); err != nil {
		return false
	}

	return true
}
